@extends('user.layouts.master')

@section('title', __(':Name | Course', ['name' => __(data_get($invest, 'summary_title_alter'))]))

@php
use App\Enums\InvestmentStatus;
use App\Enums\InterestRateType;
use App\Enums\SchemePayout;

$currency = base_currency();
$lastNote = data_get($invest->ledgers->last(), 'note');

@endphp

@section('content')
    <div class="nk-content-body">
        <div class="nk-block-head">
            <div class="nk-block-head-sub"><a href="{{ route('user.investment.dashboard') }}" class="text-soft back-to"><em class="icon ni ni-arrow-left"> </em><span>{{ __("Course") }}</span></a></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title fw-normal">{{ data_get($invest, 'summary_title_alter') }}</h3>
                    <div class="nk-block-des">
                        {{ the_inv($invest->ivx) }} <span class="badge{{ the_state($invest->status, ['prefix' => 'badge']) }} ml-1">{{ ucfirst($invest->status) }}</span>
                        @if (data_get($invest, 'cancelled_by') == auth()->id())
                            <span class="text-danger ml-1">{{ __('You have cancelled the investment plan.') }}</span>
                        @endif
                    </div>
                </div>
                @if($invest->status==InvestmentStatus::PENDING || $invest->status==InvestmentStatus::ACTIVE || $invest->status==InvestmentStatus::INACTIVE)
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools gx-3">
                        @if(data_get($invest, 'user_can_cancel') == true)
                        <li class="order-md-last">
                            <button type="button" class="btn btn-danger iv-invest-cancel" data-action="cancelled" data-confirm="yes">
                                <em class="icon ni ni-cross"></em>
                                <span>{{ __('Cancel this course') }}</span>
                            </button>
                        </li>
                        @endif
                        <li><a href="{{ route('user.investment.details', ['id' => the_hash($invest->id)]) }}" class="btn btn-icon btn-white btn-light"><em class="icon ni ni-reload"></em></a></li>
                    </ul>
                </div>
                @endif
            </div>
        </div>
        <div class="nk-block">
            @if (filled($lastNote) && data_get($invest, 'status') == InvestmentStatus::CANCELLED)
                <div class="alert alert-danger alert-icon">
                    <em class="icon ni ni-info-fill"></em> {{ $lastNote }}
                </div>
            @endif
            @if($invest->status === InvestmentStatus::ACTIVE && $invest->payout_type === SchemePayout::AFTER_MATURED)
                <div class="alert alert-primary alert-thick alert-plain">
                    <div class="alert-cta flex-wrap flex-md-nowrap g-2">
                        <div class="alert-text has-icon">
                            <em class="icon ni ni-info-fill text-primary"></em>
                            @if (data_get($invest, 'term_count') == data_get($invest, 'term_total'))
                                <p>{{ __('This course has ended.') }}</p>
                            @else
                                <p>{{ __('Course started.') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            @endif

        </div>


        @if(!empty(data_get($invest, 'profits')))
        <div class="nk-block nk-block-lg">
            <div class="nk-block-head">
                <h5 class="nk-block-title">{{ __('Course Details') }}</h5>
            </div>
            <div class="card card-bordered">
                <table class="nk-plan-tnx table">
                    <thead class="thead-light">
                    <tr>
                        <th class="tb-col-type"><span class="overline-title">{{ __('Details') }}</span></th>
                        <th class="tb-col-date tb-col-sm"><span class="overline-title">{{ __('Date & Time') }}</span></th>
                        <th class="tb-col-amount tb-col-end"><span class="overline-title">{{ __('Amount') }}</span></th>
                        <th class="tb-col-paid tb-col-end" style="width: 20px"><em class="icon ni ni-info nk-tooltip small text-soft" title="{{ __("The profit transfered into account balance or not.") }}"></em></th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td class="tb-col-type"><span class="sub-text">{{ __("Investment") }}</span></td>
                        <td class="tb-col-date tb-col-sm">
                            <span class="sub-text">{{ show_date(data_get($invest, 'order_at'), true) }}</span>
                        </td>
                        <td class="tb-col-amount tb-col-end"><span class="lead-text text-danger">- {{ amount_z(data_get($invest, 'amount'), $currency) }}</span></td>
                        <td class="tb-col-paid tb-col-end"><span class="sub-text"><em class="icon ni ni-info nk-tooltip text-soft" title="{{ __("Received from :account", ['account' => w2n(data_get($invest, 'payment_source')) ]) }}"></em></span></td>
                    </tr>

                    @foreach(data_get($invest, 'profits') as $profit)
                    <tr>
                        <td class="tb-col-type"><span class="sub-text">{{ __("Profit Earn - :rate", ['rate' => (($profit->type=='F') ? $profit->rate . ' '.$currency . ' ('.$profit->type.')' : $profit->rate . '%')]) }}</span></td>
                        <td class="tb-col-date tb-col-sm">
                            <span class="sub-text">{{ show_date(data_get($profit, 'calc_at'), true) }}</span>
                        </td>
                        <td class="tb-col-amount tb-col-end"><span class="lead-text">+ {{ amount_z($profit->amount, $currency, ['dp' => 'calc']) }}</span></td>
                        <td class="tb-col-paid tb-col-end">
                            <span class="sub-text">{!! ($profit->payout) ? '<em class="icon ni ni-info nk-tooltip text-soft" title="'. __("Batch #:id", ['id' => $profit->payout]). '"></em> ' : '' !!}</span>
                        </td>
                    </tr>
                    @endforeach

                    @if(data_get($invest, 'scheme.capital') && $invest->status==InvestmentStatus::COMPLETED)
                    <tr>
                        <td class="tb-col-type"><span class="sub-text">{{ __("Captial Return") }}</span></td>
                        <td class="tb-col-date tb-col-sm">
                            <span class="sub-text">{{ show_date(data_get($invest, 'updated_at'), true) }}</span>
                        </td>
                        <td class="tb-col-amount tb-col-end"><span class="lead-text">+ {{ amount_z(data_get($invest, 'amount'), $currency) }}</span></td>
                        <td class="tb-col-paid tb-col-end"><span class="sub-text"><em class="icon ni ni-info nk-tooltip text-soft" title="{{ __("Add to :account", ['account' => w2n(data_get($invest, 'payment_dest')) ]) }}"></em></span></td>
                    </tr>
                    @endif

                    </tbody>
                </table>
            </div>
            @if($invest->status === InvestmentStatus::ACTIVE && $invest->payout_type === SchemePayout::AFTER_MATURED)
                <div class="notes mt-2">
                    <div class="alert-note is-plain text-success">
                        <em class="icon ni ni-alert"></em>
                        @if (data_get($invest, 'term_count') == data_get($invest, 'term_total'))
                            <p>{{ __('Course Ended') }}</p>
                        @else
                            <p>{{ __('This course is active and running.') }}</p>
                        @endif
                    </div>
                </div>
            @endif
        </div>
        @endif
    </div>
@endsection

@push('scripts')
    <script>
        const routes = {
            cancelled: "{{ route('user.investment.invest.cancel', ['id' => the_hash($invest->id)]) }}"
        },
        msgs = {
            cancelled: {
                title: "{{ __('Cancel Investment?') }}",
                btn: {cancel: "{{ __('No') }}", confirm: "{{ __('Yes, Cancel') }}"},
                context: "{!! __("You cannot revert back this action, so please confirm that you want to cancel.") !!}",
                custom: "danger", type: "warning"
            }
        }
    </script>
@endpush
