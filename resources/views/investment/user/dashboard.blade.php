@extends('user.layouts.master')
@section('title', __('Investment Overview'))

@section('content')
<div class="nk-content-body">
    <div class="nk-block-head">
        <div class="nk-block-head-sub"><span>{{ __('Courses') }}</span></div>
        <div class="nk-block-between-md g-4">
            <div class="nk-block-head-content">
                <h2 class="nk-block-title fw-normal">{{ __('Your Purchased Courses') }}</h2>
                <div class="nk-block-des">
                    <p>{{ __('Summary of your courses.') }}</p>
                </div>
            </div>
            <div class="nk-block-head-content">
                <ul class="nk-block-tools gx-3">
                    <li class="order-md-last"><a href="{{ route('user.investment.invest') }}" class="btn btn-primary"><span>{{ __('Buy a course') }}</span> <em class="icon ni ni-arrow-long-right"></em></a></li>
                    <li><a href="{{ route('deposit') }}" class="btn btn-light btn-white"><span>{{ __('Deposit Funds') }}</span> <em class="icon ni ni-arrow-long-right"></em></a></li>
                </ul>
            </div>
        </div>
    </div>


    @if(!blank($pendingPlans = data_get($investments, 'pending', [])))
    <div class="nk-block nk-block-lg">
        <div class="nk-block-head-sm">
            <div class="nk-block-head-content">
                <h5 class="nk-block-title">{{ __('Pending Courses') }} <span class="count text-base">({{ count($pendingPlans) }})</span></h5>
            </div>
        </div>

        <div class="nk-plan-list">
            @foreach($pendingPlans as $plan)
                @include('investment.user.plan-row')
            @endforeach
        </div>
    </div>
    @endif

    @if(!blank($activePlans = data_get($investments, 'active', [])))
    <div class="nk-block nk-block-lg">
        <div class="nk-block-head-sm">
            <div class="nk-block-head-content">
                <h5 class="nk-block-title">{{ __('Active Courses') }} <span class="count text-base">({{ count($activePlans) }})</span></h5>
            </div>
        </div>

        <div class="nk-plan-list">
            @foreach($activePlans as $plan)
                @include('investment.user.plan-row', $plan)
            @endforeach
        </div>
    </div>
    @endif

    @if(!blank($recents))
    <div class="nk-block nk-block-lg">
        <div class="nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title">{{ __('Recently End') }} <span class="count text-base">({{ count($recents) }})</span></h5>
                </div>
                <div class="nk-block-head-content">
                    <a href="{{route('user.investment.history', 'completed')}}"><em class="icon ni ni-dot-box"></em> {{ __('Go to Archive') }}</a>
                </div>
            </div>
        </div>

        <div class="nk-plan-list">
            @foreach($recents as $plan)
                @include('investment.user.plan-row', $plan)
            @endforeach
        </div>
    </div>
    @endif
</div>
@endsection

@push('modal')
<div class="modal fade" role="dialog" id="ajax-modal"></div>
@endpush

@push('scripts')

    <script>

        var investment=JSON.parse('{!! json_encode($investChart) !!}');

        var profit=JSON.parse('{!! json_encode($profitChart) !!}');

        var dailyInvestment = {
        tooltip: false,
        legend: true,
        labels: Object.keys(investment),
        dataUnit: '{{base_currency()}}',
        stacked: true,
        lineTension: .3,
        datasets: [{
            label: "Investment",
            color: "#816bff",
            background: 'transparent',
            borderWidth: 2,
            data: Object.values(investment)
        }, {
            label: "Profit",
            color: "#c4cefe",
            background: 'transparent',
            borderWidth: 2,
            data: Object.values(profit)
        }]
    };
    </script>

@endpush
