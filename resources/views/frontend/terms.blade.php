@extends('frontend.layouts.app')

@section('title', 'A-Z Crypto College')

@section('description')

@endsection

@section('content')

    <!-- Overlay Start -->
        <div class="overlay"></div>
        <!-- Overlay End -->

        <!-- Page Banner Start -->
        <div class="section page-banner">

            <img class="shape-1 animation-round" src="/landing/assets/images/shape/shape-8.png" alt="Shape">

            <img class="shape-2" src="/landing/assets/images/shape/shape-23.png" alt="Shape">

            <div class="container">
                <!-- Page Banner Start -->
                <div class="page-banner-content">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Terms</li>
                    </ul>
                    <h2 class="title">Terms <span>& Conditions</span></h2>
                </div>
                <!-- Page Banner End -->
            </div>

            <!-- Shape Icon Box Start -->
            <div class="shape-icon-box">

                <img class="icon-shape-1 animation-left" src="/landing/assets/images/shape/shape-5.png" alt="Shape">

                <div class="box-content">
                    <div class="box-wrapper">
                        <i class="flaticon-badge"></i>
                    </div>
                </div>

                <img class="icon-shape-2" src="/landing/assets/images/shape/shape-6.png" alt="Shape">

            </div>
            <!-- Shape Icon Box End -->

            <img class="shape-3" src="/landing/assets/images/shape/shape-24.png" alt="Shape">


        </div>
        <!-- Page Banner End -->



        <!-- Contact Start -->
        <div class="section section-padding">
            <div class="container">


<div class="description-wrapper">

                                          <p>A-Z Crypto College does not offer refunds for payments made on a month-month subscription plan. To avoid being charged during a free trial promotion, you must cancel your subscription before your free trial ends. If you complete a course during the free trial period, A-Z Crypto College reserves the right to require you to pay for a one-month subscription in order to receive a course and/or specialization certificate.

Your subscription will continue on a month-to-month basis unless and until you cancel or the subscription is suspended or discontinued by A-Z Crypto College. For subscriptions to individual specializations, A-Z Crypto College will automatically discontinue your subscription at the end of the monthly period during which you earn a specialization certificate for the specialization, unless you have subscribed through a third party marketplace that restricts our ability to do so -- e.g., in-app purchases through the Apple App Store. Please visit the third party marketplace for information regarding their policies. You must cancel your subscription before your monthly renewal date to avoid the next billing.

If you cancel your subscription, cancellation will be effective at the end of the current monthly period; you will continue to have access to your subscription for the remainder of that period, but you will not receive a refund.



</p>

</div>
</div>

                        </div>

                    </div>


@endsection
