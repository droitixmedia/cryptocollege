@extends('frontend.layouts.app')
@section('title', 'A-Z Crypto College')
@section('description')
@endsection
@section('content')
<!-- Slider Start -->
<div class="section slider-section">
   <!-- Slider Shape Start -->
   <div class="slider-shape">
      <img class="shape-1 animation-round" src="/landing/assets/images/shape/shape-8.png" alt="Shape">
   </div>
   <!-- Slider Shape End -->
   <div class="container">
      <!-- Slider Content Start -->
      <div class="slider-content">
         <h4 class="sub-title">Start your favourite course</h4>
         <h2 class="main-title">Learn cryptocurrency from anywhere and build a<span> great portfolio.</span></h2>
         <p>We have a great record that goes ahead of us in crypto knowledge.</p>
         <a class="btn btn-primary btn-hover-dark" href="{{url('/dashboard')}}">Begin Today</a>
      </div>
      <!-- Slider Content End -->
   </div>
   <!-- Slider Courses Box Start -->
   <div class="slider-courses-box">
      <img class="shape-1 animation-left" src="/landing/assets/images/shape/shape-5.png" alt="Shape">
      <div class="box-content">
         <div class="box-wrapper">
            <i class="flaticon-open-book"></i>
            <span class="count">201</span>
            <p>courses</p>
         </div>
      </div>
      <img class="shape-2" src="/landing/assets/images/shape/shape-6.png" alt="Shape">
   </div>
   <!-- Slider Courses Box End -->
   <!-- Slider Rating Box Start -->
   <div class="slider-rating-box">
      <div class="box-rating">
         <div class="box-wrapper">
            <span class="count">$100 </span>
            <p>Starting From</p>
         </div>
      </div>
      <img class="shape animation-up" src="/landing/assets/images/shape/shape-7.png" alt="Shape">
   </div>
   <!-- Slider Rating Box End -->
   <!-- Slider Images Start -->
   <div class="slider-images">
      <div class="images">
         <img src="/assets/images/banner.png" alt="Slider">
      </div>
   </div>
   <!-- Slider Images End -->
   <!-- Slider Video Start -->
   <div class="slider-video">
      <img class="shape-1" src="/landing/assets/images/shape/shape-9.png" alt="Shape">
      <div class="video-play">
         <img src="/landing/assets/images/shape/shape-10.png" alt="Shape">
         <a href="https://www.youtube.com/watch?v=BRvyWfuxGuU" class="play video-popup"><i class="flaticon-play"></i></a>
      </div>
   </div>
   <!-- Slider Video End -->
</div>
<!-- Slider End -->
<div class="section section-padding mt-n10">
   <div class="container">
      <div class="row gx-10">
         <div class="col-lg-8">
            <!-- Courses Details Start -->
            <div class="courses-details">
               <div class="courses-details-images">
                  <img src="/landing/assets/images/image.jpg" alt="Courses Details">
                  <span class="tags">Video</span>
                  <div class="courses-play">
                     <img src="/landing/assets/images/courses/circle-shape.png" alt="Play">
                     <a class="play video-popup" href="https://vimeo.com/690254578"><i class="flaticon-play"></i></a>
                  </div>
               </div>

               <!-- Courses Details Tab End -->
            </div>
            <!-- Courses Details End -->
         </div>

      </div>
   </div>
</div>

<!-- All Courses Start -->
<div class="section section-padding-02">
   <div class="container">
      <!-- All Courses Top Start -->
      <div class="courses-top">
         <!-- Section Title Start -->
         <div class="section-title shape-01">
            <h2 class="main-title">Our <span>Courses</span></h2>
         </div>
         <!-- Section Title End -->
      </div>
      <!-- All Courses Top End -->
      <!-- All Courses tab content Start -->
      <div class="tab-content courses-tab-content">
         <div class="tab-pane fade show active" id="tabs1">
            <!-- All Courses Wrapper Start -->
            <div class="courses-wrapper">
               <div class="row">

                  <div class="col-lg-4 col-md-6">
                     <!-- Single Courses Start -->
                     <div class="single-courses">
                        <div class="courses-images">
                           <a href="{{url('masternodes-staking-metaverse')}}"><img src="/landing/assets/images/courses/courses-02.jpg" alt="Courses"></a>
                        </div>
                        <div class="courses-content">
                           <div class="courses-author">
                              <div class="author">
                                 <div class="author-thumb">
                                    <a href="#"><img src="/landing/assets/images/author/author-02.jpg" alt="Author"></a>
                                 </div>
                                 <div class="author-name">
                                    <a class="name" href="#">Yadin Foster</a>
                                 </div>
                              </div>
                              <div class="tag">
                                 <a href="{{url('masternodes-staking-metaverse')}}">Advanced</a>
                              </div>
                           </div>
                           <h4 class="title"><a href="{{url('masternodes-staking-metaverse')}}">
                              Masternodes,
                              Metaverse,
                              Staking,
                              NFT'S,
                              Liquidity Mining</a>
                           </h4>
                           <div class="courses-meta">
                              <span> <i class="icofont-clock-time"></i> 5 PDF's</span>
                              <span> <i class="icofont-read-book"></i> 5  Lectures </span>
                           </div>
                           <div class="courses-price-review">
                              <div class="courses-price">
                                 <span class="sale-parice">$300.00</span>
                              </div>
                              <div class="courses-review">
                                 <span class="rating-count">5.0</span>
                                 <span class="rating-star">
                                 <span class="rating-bar" style="width: 80%;"></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Single Courses End -->
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <!-- Single Courses Start -->
                     <div class="single-courses">
                        <div class="courses-images">
                           <a href="{{url('crypto-trading')}}"><img src="/landing/assets/images/courses/master.jpg" alt="Courses"></a>
                        </div>
                        <div class="courses-content">
                           <div class="courses-author">
                              <div class="author">
                                 <div class="author-thumb">
                                    <a href="{{url('crypto-trading')}}"><img src="/landing/assets/images/author/author-06.jpg" alt="Author"></a>
                                 </div>
                                 <div class="author-name">
                                    <a class="name" href="#">Kagil Pen</a>
                                 </div>
                              </div>
                              <div class="tag">
                                 <a href="{{url('crypto-trading')}}">Executive</a>
                              </div>
                           </div>
                           <h4 class="title"><a href="{{url('crypto-trading')}}">Cronodes, NFT's , One on One Sessions, Investments, Offline Training, Yield Farming.
                              </a>
                           </h4>
                           <div class="courses-meta">
                              <span> <i class="icofont-clock-time"></i> 5 PDF's</span>
                              <span> <i class="icofont-read-book"></i> 4 Lectures </span>
                           </div>
                           <div class="courses-price-review">
                              <div class="courses-price">
                                 <span class="sale-parice">$1700.00</span>
                              </div>
                              <div class="courses-review">
                                 <span class="rating-count">4.9</span>
                                 <span class="rating-star">
                                 <span class="rating-bar" style="width: 80%;"></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Single Courses End -->

                </div>
                        <!-- Single Courses End -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- All Courses Wrapper End -->
         </div>
         <div class="tab-pane fade" id="tabs2">
         </div>
         <div class="tab-pane fade" id="tabs3">
         </div>
         <div class="tab-pane fade" id="tabs4">
         </div>
         <div class="tab-pane fade" id="tabs5">
         </div>
         <div class="tab-pane fade" id="tabs6">
         </div>
         <div class="tab-pane fade" id="tabs7">
         </div>
      </div>
      <!-- All Courses tab content End -->
   </div>
</div>
<!-- All Courses End -->
<!-- Call to Action Start -->
<div class="section section-padding-02">
   <div class="container">
      <!-- Call to Action Wrapper Start -->
      <div class="call-to-action-wrapper">
         <img class="cat-shape-01 animation-round" src="/landing/assets/images/shape/shape-12.png" alt="Shape">
         <img class="cat-shape-02" src="/landing/assets/images/shape/shape-13.svg" alt="Shape">
         <img class="cat-shape-03 animation-round" src="/landing/assets/images/shape/shape-12.png" alt="Shape">
         <div class="row align-items-center">
            <div class="col-md-6">
               <!-- Section Title Start -->
               <div class="section-title shape-02">
                  <h5 class="sub-title">Start earning today</h5>
                  <h2 class="main-title">You can refer and earn <span>from 50%</span></h2>
               </div>
               <!-- Section Title End -->
            </div>
            <div class="col-md-6">
               <div class="call-to-action-btn">
                  <a class="btn btn-primary btn-hover-dark" href="contact.html">Check Your link</a>
               </div>
            </div>
         </div>
      </div>
      <!-- Call to Action Wrapper End -->
   </div>
</div>
<!-- Call to Action End -->
<!-- Testimonial End -->
<div class="section section-padding-02 mt-n1">
   <div class="container">
      <!-- Section Title Start -->
      <div class="section-title shape-03 text-center">
         <h5 class="sub-title">Students Testimonials</h5>
         <h2 class="main-title">Feedback From <span> Students</span></h2>
      </div>
      <!-- Section Title End -->
      <!-- Testimonial Wrapper End -->
      <div class="testimonial-wrapper testimonial-active">
         <div class="swiper-container">
            <div class="swiper-wrapper">
               <!-- Single Testimonial Start -->
               <div class="single-testimonial swiper-slide">
                  <div class="testimonial-author">
                     <div class="author-thumb">
                        <img src="/landing/assets/images/author/author-06.jpg" alt="Author">
                        <i class="icofont-quote-left"></i>
                     </div>
                     <span class="rating-star">
                     <span class="rating-bar" style="width: 80%;"></span>
                     </span>
                  </div>
                  <div class="testimonial-content">
                     <p>A-Z Cryptocollege is the best learning platform and what l like about it is that its flexible.</p>
                     <h4 class="name">Sara Alexander</h4>
                     <span class="designation">Trader, Tanzania</span>
                  </div>
               </div>
               <!-- Single Testimonial End -->
               <!-- Single Testimonial Start -->
               <div class="single-testimonial swiper-slide">
                  <div class="testimonial-author">
                     <div class="author-thumb">
                        <img src="/landing/assets/images/author/author-07.jpg" alt="Author">
                        <i class="icofont-quote-left"></i>
                     </div>
                     <span class="rating-star">
                     <span class="rating-bar" style="width: 80%;"></span>
                     </span>
                  </div>
                  <div class="testimonial-content">
                     <p>Great and eye opening courses at a very affordable price.</p>
                     <h4 class="name">Melissa Roberts</h4>
                     <span class="designation">Trucker, Australia</span>
                  </div>
               </div>
               <!-- Single Testimonial End -->
               <!-- Single Testimonial Start -->
               <div class="single-testimonial swiper-slide">
                  <div class="testimonial-author">
                     <div class="author-thumb">
                        <img src="/landing/assets/images/author/author-03.jpg" alt="Author">
                        <i class="icofont-quote-left"></i>
                     </div>
                     <span class="rating-star">
                     <span class="rating-bar" style="width: 80%;"></span>
                     </span>
                  </div>
                  <div class="testimonial-content">
                     <p>The referral program is very great!aw.</p>
                     <h4 class="name">Anne Van de Merwe</h4>
                     <span class="designation">IT Specialist, South Africa</span>
                  </div>
               </div>
               <!-- Single Testimonial End -->
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
         </div>
      </div>
      <!-- Testimonial Wrapper End -->
   </div>
</div>
<!-- Testimonial End -->
@endsection
