@extends('frontend.layouts.app')

@section('title', 'A-Z Crypto College')

@section('description')

@endsection

@section('content')

    <!-- Overlay Start -->
        <div class="overlay"></div>
        <!-- Overlay End -->

        <!-- Page Banner Start -->
        <div class="section page-banner">

            <img class="shape-1 animation-round" src="/landing/assets/images/shape/shape-8.png" alt="Shape">

            <img class="shape-2" src="/landing/assets/images/shape/shape-23.png" alt="Shape">

            <div class="container">
                <!-- Page Banner Start -->
                <div class="page-banner-content">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Privacy</li>
                    </ul>
                    <h2 class="title">Privacy <span>Policy</span></h2>
                </div>
                <!-- Page Banner End -->
            </div>

            <!-- Shape Icon Box Start -->
            <div class="shape-icon-box">

                <img class="icon-shape-1 animation-left" src="/landing/assets/images/shape/shape-5.png" alt="Shape">

                <div class="box-content">
                    <div class="box-wrapper">
                        <i class="flaticon-badge"></i>
                    </div>
                </div>

                <img class="icon-shape-2" src="/landing/assets/images/shape/shape-6.png" alt="Shape">

            </div>
            <!-- Shape Icon Box End -->

            <img class="shape-3" src="/landing/assets/images/shape/shape-24.png" alt="Shape">


        </div>
        <!-- Page Banner End -->



        <!-- Contact Start -->
        <div class="section section-padding">
            <div class="container">


<div class="description-wrapper">

                                          <p>Our Sites are primarily operated and managed on servers located and operated within the United States. In order to provide our products and Services to you, you may be sending your Personal Data outside of the country where you reside or are located, including to the United States. Accordingly, if you reside or are located outside of the United States, your Personal Data may be transferred outside of the country where you reside or are located, including to countries that may not or do not provide the same level of protection for your Personal Data. We are committed to protecting the privacy and confidentiality of Personal Data when it is transferred. If you reside or are located within the EEA, United Kingdom, Switzerland, or other region that offers similar protections, and such transfers occur, we take appropriate steps to provide the same level of protection for the processing carried out in any such countries as you would have within the EEA or other regions to the extent feasible under applicable law. We participate in and commit to adhering to the EU-U.S. and Swiss-U.S. Privacy Shield Frameworks when transferring data from the EEA or Switzerland to the United States. Please see our Privacy Shield Notice below for further information.

</p>

</div>
</div>

                        </div>

                    </div>


@endsection
