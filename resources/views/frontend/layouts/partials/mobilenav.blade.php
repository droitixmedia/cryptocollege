 @php
use App\Enums\UserRoles;
$style = (lang_dir() == 'rtl') ? 'apps.rtl' : 'apps';
@endphp

  <!-- Mobile Menu Start -->
        <div class="mobile-menu">

            <!-- Menu Close Start -->
            <a class="menu-close" href="javascript:void(0)">
                <i class="icofont-close-line"></i>
            </a>
            <!-- Menu Close End -->

            <!-- Mobile Top Medal Start -->
            <div class="mobile-top">

                                <p><i class="flaticon-email"></i> <a href="mailto:support@a-zcryptocollege.com">support@a-zcryptocollege.com</a></p>
            </div>
            <!-- Mobile Top Medal End -->

            <!-- Mobile Sing In & Up Start -->
            <div class="mobile-sign-in-up">
                <ul>
                    @guest
                                <li><a class="sign-in" href="{{ route('auth.login.form') }}">Sign In</a></li>
                                @else
                                   <li><a class="sign-up" href="{{ (auth()->user()->role==UserRoles::USER) ? route('dashboard') : route('admin.dashboard') }}">Dashboard</a></li>
                                @endguest
                        @if (!auth()->check() && gss('signup_allow', 'enable') == 'enable')
                                <li><a class="sign-up" href="{{ route('auth.register.form') }}">Sign Up</a></li>


                        @endif
                </ul>
            </div>
            <!-- Mobile Sing In & Up End -->

            <!-- Mobile Menu Start -->
            <div class="mobile-menu-items">
                <ul class="nav-menu">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ route('investments') }}">Buy a course</a></li>


                    <li><a href="{{ url('contact-us') }}">Contact</a></li>


                </ul>

            </div>
            <!-- Mobile Menu End -->

            <!-- Mobile Menu End -->
            <div class="mobile-social">
                <ul class="social">
                    <li><a href="#"><i class="flaticon-facebook"></i></a></li>
                    <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                    <li><a href="#"><i class="flaticon-skype"></i></a></li>
                    <li><a href="#"><i class="flaticon-instagram"></i></a></li>
                </ul>
            </div>
            <!-- Mobile Menu End -->

        </div>
        <!-- Mobile Menu End -->
