@extends('frontend.layouts.app')

@section('title', 'A-Z Crypto College')

@section('description')

@endsection

@section('content')

<!-- Page Banner Start -->
         <div class="section page-banner">
            <img class="shape-1 animation-round" src="/landing/assets/images/shape/shape-8.png" alt="Shape">
            <img class="shape-2" src="/landing/assets/images/shape/shape-23.png" alt="Shape">
            <div class="container">
               <!-- Page Banner Start -->
               <div class="page-banner-content">
                  <ul class="breadcrumb">
                     <li><a href="#">Home</a></li>
                     <li class="active">Course Details</li>
                  </ul>
                  <h2 class="title">Course <span> Details</span></h2>
               </div>
               <!-- Page Banner End -->
            </div>
            <!-- Shape Icon Box Start -->
            <div class="shape-icon-box">
               <img class="icon-shape-1 animation-left" src="/landing/assets/images/shape/shape-5.png" alt="Shape">
               <div class="box-content">
                  <div class="box-wrapper">
                     <i class="flaticon-badge"></i>
                  </div>
               </div>
               <img class="icon-shape-2" src="/landing/assets/images/shape/shape-6.png" alt="Shape">
            </div>
            <!-- Shape Icon Box End -->
            <img class="shape-3" src="/landing/assets/images/shape/shape-24.png" alt="Shape">
            <img class="shape-author" src="/landing/assets/images/author/author-11.jpg" alt="Shape">
         </div>
         <!-- Page Banner End -->
         <!-- Courses Start -->
         <div class="section section-padding mt-n10">
            <div class="container">
               <div class="row gx-10">
                  <div class="col-lg-8">
                     <!-- Courses Details Start -->
                     <div class="courses-details">
                        <div class="courses-details-images">
                           <img src="/landing/assets/images/courses/master.jpg"  alt="Courses Details">
                           <span class="tags">Executive</span>
                           <div class="courses-play">
                              <img src="/landing/assets/images/courses/circle-shape.png" alt="Play">

                           </div>
                        </div>
                        <h2 class="title">One on one training sessions zoom  or Offline Physical Training sessions,
investment to fast & highly rewarding platforms,
cronodes
,yield farming


 .</h2>

                        <!-- Courses Details Tab Start -->
                        <div class="courses-details-tab">

                           <!-- Details Tab Content Start -->
                           <div class="details-tab-content">
                              <div class="tab-content">
                                 <div class="tab-pane fade show active" id="description">
                                    <!-- Tab Description Start -->
                                    <div class="tab-description">
                                       <div class="description-wrapper">
                                          <h3 class="tab-title">Description:</h3>
                                          <p>
This specialization introduces blockchain, a revolutionary technology that enables peer-to-peer transfer of digital assets without any intermediaries, and is predicted to be just as impactful as the Internet. More specifically, it prepares learners to program on the Ethereum blockchain.

</p>

                                       </div>
                                       <div class="description-wrapper">
                                          <h3 class="tab-title">Topics:</h3>
                                          <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <th><i class="icofont-book-alt"></i><span>:</span></th>
                                                                <td>Cronodes</td>
                                                            </tr>
                                                            <tr>
                                                                <th><i class="icofont-book-alt"></i><span>:</span></th>
                                                                <td>NFT's </td>
                                                            </tr>
                                                            <tr>
                                                                <th><i class="icofont-book-alt"></i><span>:</span></th>
                                                                <td>Yield Farming</td>
                                                            </tr>


                                                        </tbody>
                                                    </table>


                                       </div>
                                       <div class="description-wrapper">
                                          <h3 class="tab-title">Certification:</h3>
                                          <p>You will get our college certificate after completing this course.</p>
                                       </div>
                                    </div>
                                    <!-- Tab Description End -->
                                 </div>

                              </div>
                           </div>
                           <!-- Details Tab Content End -->
                        </div>
                        <!-- Courses Details Tab End -->
                     </div>
                     <!-- Courses Details End -->
                  </div>
                  <div class="col-lg-4">
                     <!-- Courses Details Sidebar Start -->
                     <div class="sidebar">
                        <!-- Sidebar Widget Information Start -->
                        <div class="sidebar-widget widget-information">
                           <div class="info-price">
                              <span class="price">$1700</span>
                           </div>
                           <div class="info-list">
                              <ul>
                                 <li><i class="icofont-man-in-glasses"></i> <strong>Instructor</strong> <span>Kagil Pen</span></li>
                                 <li><i class="icofont-clock-time"></i> <strong>Topics</strong> <span>3</span></li>

                                 <li><i class="icofont-bars"></i> <strong>Level</strong> <span>Executive</span></li>
                                 <li><i class="icofont-book-alt"></i> <strong>Language</strong> <span>English</span></li>
                                 <li><i class="icofont-certificate-alt-1"></i> <strong></strong> <span></span></li>
                              </ul>
                           </div>
                           <div class="info-btn">
                              <a href="#" class="btn btn-primary btn-hover-dark">Buy Course</a>
                           </div>
                        </div>
                        <!-- Sidebar Widget Information End -->
                        <!-- Sidebar Widget Share Start -->
                        <div class="sidebar-widget">
                           <h4 class="widget-title">Share Course:</h4>
                           <ul class="social">
                              <li><a href="#"><i class="flaticon-facebook"></i></a></li>
                              <li><a href="#"><i class="flaticon-linkedin"></i></a></li>
                              <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                              <li><a href="#"><i class="flaticon-skype"></i></a></li>
                              <li><a href="#"><i class="flaticon-instagram"></i></a></li>
                           </ul>
                        </div>
                        <!-- Sidebar Widget Share End -->
                     </div>
                     <!-- Courses Details Sidebar End -->
                  </div>
               </div>
            </div>
         </div>
         <!-- Courses End -->
         <!-- Download App Start -->
         <div class="section section-padding download-section">
            <div class="app-shape-1"></div>
            <div class="app-shape-2"></div>
            <div class="app-shape-3"></div>
            <div class="app-shape-4"></div>
            <div class="container">
               <!-- Download App Wrapper Start -->
               <div class="download-app-wrapper mt-n6">
                  <!-- Section Title Start -->
                  <div class="section-title section-title-white">
                     <h5 class="sub-title">Ready to go mobile?</h5>
                     <h2 class="main-title">Coming Soon!.</h2>
                  </div>
                  <!-- Section Title End -->
                  <img class="shape-1 animation-right" src="/landing/assets/images/shape/shape-14.png" alt="Shape">
                  <!-- Download App Button End -->
                  <div class="download-app-btn">
                     <ul class="app-btn">
                        <li><a href="#"><img src="/landing/assets/images/google-play.png" alt="Google Play"></a></li>
                        <li><a href="#"><img src="/landing/assets/images/app-store.png" alt="App Store"></a></li>
                     </ul>
                  </div>
                  <!-- Download App Button End -->
               </div>
               <!-- Download App Wrapper End -->
            </div>
         </div>
         <!-- Download App End -->

@endsection
